<expr> -> ( <term> | <expr> ) {(+ | -) <term>}
<term> -> ( <factor> | <term> ) {(* | /) <factor>}
<factor> -> id | int_constant | ( <expr> )

Normalized:

E => TOT | EOT | T
T => FMF | TMF | F
F => LER | I

Where (Token Codes)

E => <expr>
T => <term>
F => <factor>
O => ( + | - )
M => ( * | / )
I => id | int_constant

id => [A..Z, a..z] single case-insensitive character
int_constant => [0..9] single digit

Order of processing (STEP):

1. LER => F
2. FMF => T
3. TOT => E
4. F => T
5. T => E

Restart cycle once a change is made (i.e. a match is found for 2, replace, start back with 1).

Algorithm:
1.  Parse string and convert to Token Codes, removing all white spaces.
2.  While match exists
3.     Foreach STEP
4.        if match
5.           replace first match
6.           break out of Foreach
7.        end if
8.     end foreach
9.  End while
10. If Result is "E" 
11.    print "ACCEPT"
12. Else 
13.    print "REJECT"
14. End if

Sample Parse (ACCEPT)

1.  Input:               ( ( A + B ) + C ) / 0
2.  Convert to tokens:   L L I O I R O I R M I
3.  Strip whitespace:    LLIOIROIRMI
4.  Convert I to F:      LLFOFROFRMF
5.  Match F => T:        LLTOFROFRMF
6.  Match F => T:        LLTOTROFRMF
7.  Match TOT => E:      LLEROFRMF
8.  Match LER => F:      LFOFRMF
9.  Match F => T:        LTOFRMF
10. Match F => T:        LTOTRMF
11. Match TOT => E:      LERMF
12. Match LER => F:      FMF
13. Match FMF => T:      T
14. Match T => E:        E
15. No matches           E
16. Result == "E" => Print "ACCEPT"

Sample Parse (REJECT)

1.  Input:               - B ( C )
2.  Convert to tokens:   O I L I R
3.  Strip whitespace:    OILIR
4.  Convert I to F:      OFLFR
5.  Match F => T:        OTLFR
6.  Match F => T:        OTLTR
7.  Match T => E:        OELTR
8.  Match T => E:        OELER
9.  Match LER => E:      OEE
10. No matches           OEE
11. Result != "E" => Print "REJECT"


