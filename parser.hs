-- Simple Parser in a Functional Language
-- Final Project - CS 441 Fall 2013
-- Shawn Mathes, smbr5@mail.umkc.edu
-- December 8, 2013

-- |This program is a simple parser to assess expressions under the grammar for simple
-- arithmetic expressions.  It reads in lines from a plain text file named @\"INPUT.TXT\"@ 
-- in the same directory as program.  Each line is validated and is provided with \"ACCEPT\" 
-- or \"REJECT\" depending on its acceptance in parsing.  It does not evaluate the semantics 
-- of the expression, only whether the expressions are mechanically well-formed according to 
-- the specified grammar.
--
-- Parsing is performed using a bottom-up approach.  The following EBNF description of simple 
-- arithmetic expressions is used:
--
-- @
--   \<expr\> -\> \<term\> {(+ | -) \<term\>}
--   \<term\> -\> \<factor\> {(* | /) \<factor\>}
--   \<factor\> -\> id | int_constant | ( \<expr\> )
-- @
--
-- /Source: Sebesta, Robert W. \"Concepts of Programming Languages\". 10th Ed. pg 182./
--
-- Assumptions:
--
-- * The expressions will be in a plain text file called @\"INPUT.TXT\"@ in the same directory
-- as the program file.
--
-- * The text file will contain one expression per line.  Each line with an expression will end
-- with a newline ('\\n').  A blank (0-length) line at the end of the file marks the end of input.
--
-- * All identifiers will be single alphabetic letters.  Identifiers are case-sensitive.
--
-- * All numeric values will be single digit integers.
--
-- * Output is to the screen: \"ACCEPT\" if the line is a valid expression under the grammar,
-- or \"REJECT\" if it is not.  No diagnostic or tracking output is available.
--
-- * Reason for rejection will not be shown.
module Main where

import Data.Char
import Text.Regex.Posix

-- |Grammar rules.  Grammar was normalized to the following:
--
-- @
--   E -\> E O T | T O E | T O T | T
--   T -\> F M T | T M F | F M F | F
--   F -\> L E R
-- @
--
-- /Note: See lookup for resolving characters to O, M, and F./
--
rules :: [(String,String)]
rules = [("(L.R)", ""),      -- Special case rule to process rules within parenthesis first.
            ("(LER)", "F"),
            ("(TMF)", "T"),
            ("(FMT)", "T"),
            ("(FMF)", "T"),
            ("(EOT)", "E"),
            ("(TOE)", "E"),
            ("(TOT)", "E"),
            ("(F)", "T"),
            ("(T)", "E")]

-- |Read file and validate each line in text
main :: IO ()
main = do 
 contents <- readFile "input.txt"
 putStr (unlines (validateLines (lines contents)))
 where validateLines = map validateLine
       validateLine "" = "" 
       validateLine l = l ++ " ... " ++ validate l

-- |Validate string for valid parse
validate :: String -> String
validate s = if parse ( Main.lex s ) == "E"
             then "ACCEPT"
             else "REJECT"

-- |Parses a string recursively using the grammar rules until no match is found.  If a
-- match is found, then it is replaced with the associated handle.
parse :: String -> String
parse s
  | contains s ( head rules ) = parse ( parsein ( regxmatch s ( fst ( head rules ) ) ) )
  | contains s ( rules !! 2 ) = parse ( regxrep s ( rules !! 2 ) )
  | contains s ( rules !! 3 ) = parse ( regxrep s ( rules !! 3 ) )
  | contains s ( rules !! 4 ) = parse ( regxrep s ( rules !! 4 ) )
  | contains s ( rules !! 5 ) = parse ( regxrep s ( rules !! 5 ) )
  | contains s ( rules !! 6 ) = parse ( regxrep s ( rules !! 6 ) )
  | contains s ( rules !! 7 ) = parse ( regxrep s ( rules !! 7 ) )
  | contains s ( rules !! 8 ) = parse ( regxrep s ( rules !! 8 ) )
  | contains s ( rules !! 9 ) = parse ( regxrep s ( rules !! 9 ) )
  | otherwise = s
  where parsein (a, b, c) = a ++ regxrep ( [ head b ] ++ parse ( tail ( init b ) ) ++ [ last b ] ) ( rules !! 1 ) ++ c
        contains input (pattern, _) = input =~ pattern :: Bool
        regxrep input (pattern, rep) = replace ( regxmatch input pattern ) rep
        regxmatch input pattern = input =~ pattern :: (String,String,String)
        replace (a, _, c) d = a ++ d ++ c

-- |A simple lexical analyzer for arithmetic expressions
lex :: String -> String
lex = concatMap Main.lookup

-- |A function to lookup operators, parenthesis, letter, and digit and
-- return the token.
--
-- @
--   F -\> [A..Z] | [a..z] | [0..9]
--   O -\> + | -
--   M -\> * | \/
-- @
--
-- Note: A space is returned as an empty character and any other character is returned as /?/.
lookup :: Char -> String
lookup x
  | isAlphaNum x = "F"
  | otherwise = 
    case x of 
        '(' -> "L"
        ')' -> "R"
        '+' -> "O"
        '-' -> "O"
        '*' -> "M"
        '/' -> "M"
        ' ' -> ""
        otherwise -> "?"
